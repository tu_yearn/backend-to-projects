import { Request, Response } from 'express'

function indexWelcome(req: Request, res: Response): Response {
   return res.json('Welcome to the Api');
}

export default indexWelcome;
