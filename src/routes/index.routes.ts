import { Router } from 'express';
import indexWelcome from '../controllers/index.controllers'

const routes = Router();

routes.use('/', indexWelcome);

export default routes;
