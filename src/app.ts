import express, { Application } from 'express';
import morgan from 'morgan';
import path from 'path';
import cors from 'cors';

// Routes
import IndexRoutes from './routes/index.routes';

export class App {
    private app: Application;

    constructor(
        private port?: number | string,
        private folder?: string
    ) {
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
    }

    private settings() {
        this.app.set('port', this.port || process.env.PORT || 3200);
        this.app.set('folder', this.folder || process.env.DIST_FOLDER_UNISWAP || '../dist_uniswap');
        this.app.set('folder2', this.folder || process.env.DIST_FOLDER_REN || '../dist_ren');
    }

    private middlewares() {
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
    }

    private routes() {
        this.app.use('/api/v1',IndexRoutes);

        // PONGO A SERVIR LA RUTA DE MIS ARCHIVOS
        this.app.use(express.static(path.join(__dirname, this.app.get('folder'))));
        this.app.use(express.static(path.join(__dirname, this.app.get('folder2'))));

        // DEVUELVO MI INDEX.HTML CUANDO SE APUNTE A UNA RUTA ESPECIFICA

        this.app.get('/uniswap', (req, res) => {
            res.sendFile(path.join(__dirname, this.app.get('folder') + '/index.html'));
        });

        this.app.get('/*', (req, res) => {
            res.sendFile(path.join(__dirname, this.app.get('folder2') + '/index.html'));
        });

    }

    async listen(): Promise < void> {
        this.app.listen(this.app.get('port'));
        console.log('Server on port', this.app.get('port'));
    }

}
